import { createApp } from 'vue'
// Import required parameters
import router from "./lib/router";
import store from "./lib/store";
// Import css file
import './assets/index.css';

//
const app = createApp({})
//
app.use(router)
app.use(store)
//
app.mount('#app')


