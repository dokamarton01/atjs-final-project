// Importing required APIs and functions
import {
    validateGuess,
    createCleanCard,
    getCardAPI,
    getDeckAPI,
} from '@/lib/game';

// Exporting actions
export default {
    // Setting next guess (new) !!!
    setNextGuess({ commit }, color) {
        commit('setNextGuess', color)
    },

    // New deck
    async getDeck({ commit }) {
        // accessing deck_id variable directly from json
        const { deck_id } = await fetch(getDeckAPI()).then((r) => r.json());
        // Separate function to change variable
        commit('setDeckId', deck_id)
    },

    // Drawing a card
    async drawCard({ state, commit, getters }) {
        const { cards } = await fetch(getCardAPI(state.guesser.deckId)).then((r) =>
            r.json()
        );

        // Drawn card (new)
        const card = cards[0]

        // Validating with guess
        commit('incrementGuesses')
        if(validateGuess(card, state.guesser.nextGuess)) {
            commit('incrementPoints');
        }

        commit('pushNewCard', createCleanCard(card));
    },
};