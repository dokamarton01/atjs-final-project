// Store capabilities
import { createStore } from 'vuex';
// Importing store parameters
import mutations from './mutations';
import actions from './actions';

// Create a new store instance.
const store = createStore({
    state () {
        return {
            guesser: {
                deckId: undefined,
                cards: [],
                points: 0,
                guesses: 0,
                nextGuess: undefined,
            },
        };
    },
    mutations,
    actions,
});

export default store;