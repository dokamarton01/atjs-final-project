const dataset = [
    'red',
    'blue',
    'black',
    'red',
    'red',
    'black',
    'black',
    'red',
    'black',
    'red',
    'red',
    'red',
    'red',
    'black',
    'red',
    'blue',
    'red',
    'blue',
    'red',
    'red',
    'blue',
    'red',
    'blue',
    'red',
    'red',
    'red',
    'blue',
    'red',
    'purple',
    'red',
    'red',
];

// more flexible than foreach
const reducer = (acc, currentValue) => {
    // console.log(acc, currentValue)
    if(acc[currentValue]){
        acc[currentValue]++
    }
    else{
        acc[currentValue] = 1
    }

    return acc
}

// Calling thr reducer and putting it into an object
console.log(dataset.reduce(reducer, {}))